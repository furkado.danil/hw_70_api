<?php

namespace Tests\Unit;

use App\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticle()
    {
        $data = [
          'title' => $this->faker->word,
          'body' => $this->faker->text,
          'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('articles.store'),$data);

        $response->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['title'], $resp->data->title);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);

    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleValidation()
    {
        $data = [
            'title' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('articles.store'), $data);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);

        $resp = json_decode($response->getContent());

        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));

        $this->assertTrue(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyValidation()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => '',
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('articles.store'), $data);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);

        $resp = json_decode($response->getContent());

        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));

        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDestroyArticle()
    {
        $response = $this->json('delete', route('articles.destroy', ['article' => $this->article]));

        $response->assertNoContent();
    }
}
