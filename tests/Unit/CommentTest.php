<?php

namespace Tests\Unit;



use App\Comment;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        $data = [
            'article_id' => $this->article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('comments.store'),$data);

        $response->assertCreated();
        $this->assertDatabaseHas('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['article_id'], $resp->data->article->id);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testBodyValidation()
    {
        $data = [
            'article_id' => $this->article->id,
            'body' => '',
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('comments.store'), $data);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);

        $resp = json_decode($response->getContent());

        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));

        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessDestroyComment()
    {
        $response = $this->json('delete', route('comments.destroy', ['comment' => $this->comment]));

        $response->assertNoContent();
    }
}
